import Fastify from "fastify";

const fastify = Fastify({
  logger: true,
  ignoreTrailingSlash: true,
});

fastify.register(require("./db/init"));
fastify.register(require("./routes/notes.route"));

const start = async () => {
  try {
    await fastify.listen(3000, "0.0.0.0");
  } catch (err) {
    fastify.log.error(err);
    process.exit(1);
  }
};
start();
