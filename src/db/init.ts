import fastifyPlugin from "fastify-plugin";
import fastifyMongo, { FastifyMongodbOptions } from "fastify-mongodb";

async function dbConnector(fastify: any, options: any) {
  fastify.register(fastifyMongo, {
    url: "mongodb://localhost:27017/fastify_pure",
    auth: {
      username: "root",
      password: "example",
    },
    authSource: "admin",
  } as FastifyMongodbOptions);
}

// Wrapping a plugin function with fastify-plugin exposes the decorators
// and hooks, declared inside the plugin to the parent scope.
module.exports = fastifyPlugin(dbConnector);
