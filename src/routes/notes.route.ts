import { ObjectId } from "fastify-mongodb";
import { NotFound } from "http-errors";
import { createNoteSchema } from "../schemas/create-note.schema";
import { updateNoteSchema } from "../schemas/update-note.schema";

async function routes(fastify: any, options: any) {
  const notes = fastify.mongo.db.collection("notes");

  fastify.get("/notes", async (request: any, reply: any) => {
    const result = await notes.find().toArray();
    if (result.length === 0) {
      throw new NotFound("No documents found");
    }
    return reply.send(result);
  });

  fastify.get("/notes/:id", async (request: any, reply: any) => {
    const result = await notes.findOne({
      _id: new ObjectId(request.params.id),
    });
    if (!result) {
      throw new NotFound("No document found");
    }
    return reply.send(result);
  });

  fastify.post(
    "/notes",
    { schema: createNoteSchema },
    async (request: any, reply: any) => {
      const result = await notes.insertOne({
        title: request.body.title,
        description: request.body.description,
      });
      return reply.send(result);
    }
  );

  fastify.patch(
    "/notes/:id",
    { schema: updateNoteSchema },
    async (request: any, reply: any) => {
      const result = await notes.updateOne(
        { _id: new ObjectId(request.params.id) },
        {
          $set: {
            ...request.body,
          },
        }
      );
      return reply.send(result);
    }
  );

  fastify.delete("/notes/:id", async (request: any, reply: any) => {
    const result = await notes.deleteOne({
      _id: new ObjectId(request.params.id),
    });
    if (result.deletedCount === 0) {
      throw new NotFound("No document found");
    }
    return reply.send(result);
  });
}

module.exports = routes;
