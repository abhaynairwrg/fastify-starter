const notesBodyUpdateJsonSchema = {
  type: "object",
  required: [],
  properties: {
    title: { type: "string" },
    description: { type: "string" },
  },
};

export const updateNoteSchema = {
  body: notesBodyUpdateJsonSchema,
};
